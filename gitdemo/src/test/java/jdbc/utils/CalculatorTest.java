package jdbc.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    @CsvSource({
         "1,2,7",
         "2,3,5",
         "4,150,5"
    })
    @ParameterizedTest
    void sum(int a, int b, int s) {
        Calculator calculator = new Calculator();
        calculator.sum(a,b);
    }


    @ParameterizedTest
    @MethodSource("initDataForTestCalc")
    void sumAdvanced(int a, int b, int result) {
        Calculator calculator = new Calculator();
        assertEquals(result, calculator.sum(a,b));
    }

    public static Stream<Arguments> initDataForTestCalc() {
        List<Arguments> list = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            int a = (int) (Math.random() * 1000);
            int b = (int) (Math.random() * 1000);
            int result = a + b;
            if (i == 98) {
                result = 0;
            }
            list.add(Arguments.arguments(a, b, result));
        }
        return list.stream();
    }
}