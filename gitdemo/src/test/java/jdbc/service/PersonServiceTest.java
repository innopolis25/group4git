package jdbc.service;

import jdbc.model.Car;
import jdbc.model.Person;
import jdbc.utils.JdbcConnection;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;


public class PersonServiceTest {

    Statement statement;
    PersonService personService;

    @BeforeEach
    public void init() throws SQLException {
        System.out.println("before-each");
        statement = JdbcConnection.getStatement();
        personService = new PersonService();
    }

    @BeforeAll
    public static void initAll() {
        System.out.println("Before-ALL");
    }

    @Test
    public void findPersonById() throws SQLException {
        System.out.println("start findPersonById");
        Person person = personService.findPersonById(2L, statement);
        assertNotNull(person);
        assertEquals("Mask", person.getName());
        assertThrows(SQLException.class, () -> {
            throw new SQLException();
        });
    }

    @Test
//    @Ignore("Еще не готовый метод")
    public void buyCar() throws SQLException {
        System.out.println("start buyCar");
        boolean result = personService.buyCar(initPerson(), initCar(), statement);
        assertTrue(result);
    }

    private Person initPerson() {
        Person person = new Person("Petr1", 45);
        return person;
    }

    private Car initCar() {
        Car car = new Car("Ferrari1", "red");
        return car;
    }
}