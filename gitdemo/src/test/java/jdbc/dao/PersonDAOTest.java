package jdbc.dao;

import jdbc.model.Person;
import jdbc.utils.JdbcConnection;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class PersonDAOTest {

    @Test
    public void findPersonById() throws SQLException {
        Statement statement = JdbcConnection.getStatement();
        PersonDAO dao = new PersonDAO();
        Person person = dao.findPersonById(6L, statement);
        assertNotNull(person);
    }
}