package exception;

import collection.Human;
import oopAdvanced.Animal;
import oopAdvanced.Cat;
import oopAdvanced.HomeCat;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


// Throwable - Error/Exception - checked/unchecked
public class MainExcTest {

    public static void main(String[] args) {

        try {
            System.out.println("try start"); // 1
            readFile();
            System.out.println("after file"); // 2
            excTesst();
            System.out.println("after / 0"); // 3

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException | ArithmeticException e) {
            System.out.println("Файл не найден в корне проекта"); // 4
        } catch (IOException e) {
            System.out.println("Ошибка ввода вывода");
        }
//
//        System.out.println("end of main"); // 5
        // try/catch/finally  throws throw

        // try-with-resources
//        try (BufferedReader bf = new BufferedReader(new FileReader("test.txt"))) {
//            excTesst();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        System.out.println("end of main"); // 2
    }

    public static void readFile() throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader("test.txt"));
        String s;
        while ((s = bf.readLine()) != null) {
            System.out.println("Inside file: " + s);
            if ("Hello".equals(s)) {
                try {
                    throw new HelloException("В файле найдена строка:" + s);
                } catch (HelloException e) {
                    System.out.println("readFile. HelloException");
                    throw new ArithmeticException();
                }
            }
        }
        bf.close();
    }

    public static void excTesst() {
        int a = 10;
        a -= 10;
        int b = 45 / a; // Деление на 0
    }

    public static void arrayIndex() {
        int[] ints = new int[10];
        ints[100] = 10;
    }

    public static void classCast() {
        Cat cat = new Cat("str", "str");
        HomeCat animal = (HomeCat) cat;
    }

    public static void nullPointer() {
        String s = null;
        s.length();
    }


}
