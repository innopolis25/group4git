package exception;

import java.io.IOException;

public class HelloException extends Exception {

    public HelloException(String message) {
        super(message);
    }

    public HelloException() {
        super();
    }
}
