package lambda;

public class LapTop implements FunctionalTest  {

    private String brand;
    private String OC;

    @Override
    public void howToStart() {
        System.out.println("Starting quick in LapTop");
    }

    public void uniqueForLaptopMethod(String s) {
        System.out.println(s);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getOC() {
        return OC;
    }

    public void setOC(String OC) {
        this.OC = OC;
    }

    @Override
    public String toString() {
        return "Laptop(brand=" + brand + ", OC=" + OC;
    }
}
