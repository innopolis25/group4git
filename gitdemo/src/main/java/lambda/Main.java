package lambda;

public class Main {

    final int[] array = new int[10];

    public static void main(String[] args) {
        // 1
//        LapTop lapTop = new LapTop();
//        lapTop.howToStart();
//        lapTop.setOC("Linux");
//        lapTop.setBrand("Apple");
//        lapTop.uniqueForLaptopMethod();
//
//        // 2
        FunctionalTest functionalTest = new FunctionalTest() {
            @Override
            public void howToStart() {
                System.out.println("Start in anonimous");
                this.howToStart();
            }
        };
//        functionalTest.howToStart();
//
//        // 3
//        FunctionalTest functionalTest1 = () -> System.out.println("start in lambda");
//        functionalTest1.howToStart();
//
//
//
//        FunctionalTest testForPC = () -> System.out.println("Start PC");
//        PersonalComputer pc = new PersonalComputer();
//        pc.startUp(testForPC);
//
//        SeveralMethodsFuncInt test1 = r -> {
//            int length = r.length();
//            String s = "test " + length;
//            return s;
//        };
//
//        System.out.println(test1.doSmthWithStr("12345"));
//        test1.doSmthWithStr("asdf");
//        test1.doSmthWithStr("Name LAstNAme");

        // нужен метод для работы с числами (четное - true или нет - false)

        //   ::
//        LapTop lapTop = new LapTop();
//        NewOperatorTest test = System.out::println; // new Laptop(); Laptop::new - Stream Api
//        NewOperatorTest test2 = lapTop::uniqueForLaptopMethod;
//        test.test("Hello WOrld");
        NewOperatorTest test3 = LapTop::new;
        NewOperatorTest test4 = () -> new LapTop();
        NewOperatorTest test5 = StringUtil::foo2;
    }

    public void testThis() {
        LapTop lapTop = new LapTop();
        lapTop.setOC("apple");
        lapTop = new LapTop();
        LapTop finalLapTop = lapTop;
        NewOperatorTest test6 = () -> {
            System.out.println(finalLapTop);
            this.testThis();
            return finalLapTop;
        };
    }
}
