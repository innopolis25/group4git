package lambda;

@FunctionalInterface
public interface CheckNumber {
    boolean isSomething(int a);
}
