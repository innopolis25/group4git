package lambda;

public class ArrayUtil {

    // 2. 1234 -> 1+2+3+4 = 10 -> 10 isEven?
    //    1234 - "1234"

    static int[] modifyInt(int[] array, CheckNumber check) {
        int[] newArr = new int[array.length];
        int j = 0;

        for (int i : array) {
            if (check.isSomething(i)) {
                newArr[j] = i;
                j++;
            }
        }
        return newArr; // вернуть массив ОТФИЛЬТРОВАННЫЙ
    }
}
