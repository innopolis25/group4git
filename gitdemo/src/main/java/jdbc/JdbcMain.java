package jdbc;

import jdbc.model.Car;
import jdbc.model.Person;
import jdbc.service.PersonService;
import jdbc.utils.JdbcConnection;

import java.sql.*;

public class JdbcMain {

    public static void main(String[] args) throws SQLException {
        Statement statement = JdbcConnection.getStatement();
        PersonService personService = new PersonService();
        personService.buyCar(initPerson(), initCar(), statement);
    }

    private static Person initPerson() {
        Person person = new Person("Petr", 45);
        return person;
    }

    private static Car initCar() {
        Car car = new Car("Ferrari", "red");
        return car;
    }

}
