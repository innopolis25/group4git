package jdbc.dao;

import jdbc.model.Car;
import jdbc.utils.CarMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CarDAO {

    CarMapper carMapper = new CarMapper();

    public Car findPersonById(long id, Statement statement) {
        Car car = null;
        try {
            ResultSet resultSet = statement
                    .executeQuery("select * from car where id = " + id);
            car = carMapper.createCar(resultSet);
        } catch (SQLException e) {
            System.out.println("Error getting data. car.id = " + id);
        }
        return car;
    }

    public boolean insertCarForPerson(Car car, Statement statement) {
        int i = 0;
        try {
            i = statement.executeUpdate("insert into car (model, color)"
                    + " values ('"+ car.getModel() + "','" + car.getColor() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

    public Car getLastInsertedCar(Statement statement) {
        ResultSet resultSet = null;
        Car car = null;
        try {
            resultSet = statement.executeQuery("select * from car order by id desc limit 1");
            car = carMapper.createCar(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return car;
    }

    public void updateCarPersonId(Long personId, Long carId, Statement statement) {
        try {
            statement.executeUpdate("update car set person_id = " + personId + " where id = " + carId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
