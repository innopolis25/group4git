package jdbc.dao;

import jdbc.model.Person;
import jdbc.utils.PersonMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// TDD
public class PersonDAO {

    PersonMapper personMapper = new PersonMapper();

    public Person findPersonById(long id, Statement statement) {
        Person person = null;
        try {
            ResultSet resultSet = statement
                    .executeQuery("select * from person where id = " + id);
            person = personMapper.createPerson(resultSet);
        } catch (SQLException e) {
            System.out.println("Error getting data. person.id = " + id);
        }
        return person;
    }

    public boolean insertPerson(Person person, Statement statement) {
        int i = 0;
        // Artem; truncate table person;
        try {
            i = statement.executeUpdate("insert into person (name, age) "
                    + " values ('" + person.getName() + "','" + person.getAge() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

    public Person getLastInsertedPerson(Statement statement) {
        ResultSet resultSet = null;
        Person person = null;
        try {
            resultSet = statement.executeQuery("select * from person order by id desc limit 1");
            person = personMapper.createPerson(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    public void updatePersonCarId(Long personId, Long carId, Statement statement) {
        try {
            statement.executeUpdate("update person set car_id = " + carId + " where id = " + personId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
