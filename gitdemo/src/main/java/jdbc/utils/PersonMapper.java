package jdbc.utils;

import jdbc.model.Car;
import jdbc.model.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper {

    public Person createPerson(ResultSet rs) throws SQLException {
        Person person = null;
        while (rs.next()) {
            long id = rs.getInt("id");
            String name = rs.getString("name");
            int age = rs.getInt("age");
            long carId = rs.getLong("car_id");
            person = new Person(id, name, age, new Car(carId));
        }
        return person;
    }
}
