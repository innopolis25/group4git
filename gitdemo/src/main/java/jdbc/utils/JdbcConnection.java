package jdbc.utils;

import org.apache.log4j.Logger;

import java.sql.*;

public class JdbcConnection {
    private static final Logger log = Logger.getLogger(JdbcConnection.class);

    private final static String URL = "jdbc:postgresql://localhost:5436/postgres";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "r_20k40M11l35K40";

    public static Statement getStatement() throws SQLException {
        Connection connection = getConnection();
        if (connection == null) {
            return null;
        }
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            log.error("Error creating statement");
        }
        // select * from person where id = 560 and name = 'Artem; drop table'?
//        PreparedStatement ps = connection.prepareStatement("select * from person where id = ? and name = ?");
//        ps.setLong(1, 560L);
//        ps.setString(2, "Artem; drop table");
//        ps.execute();
//
//        PreparedStatement ps1 = connection.prepareStatement("insert into person (name, age) values (?,?)");
//        for (int i = 0; i < 10; i++) {
//            ps1.setString(1, "Name" + i);
//            ps1.setInt(2, i + 15);
//            ps1.addBatch();
//        }
//        ps1.executeBatch();
        System.out.println("Connection established");
        return statement;
    }

    private static Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager
                    .getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Connection unavailable");
        }
        return connection;
    }

}
