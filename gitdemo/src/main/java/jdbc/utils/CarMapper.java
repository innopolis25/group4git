package jdbc.utils;

import jdbc.model.Car;
import jdbc.model.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CarMapper {

    public Car createCar(ResultSet rs) throws SQLException {
        Car car = null;
        while (rs.next()) {
            long id = rs.getInt("id");
            String model = rs.getString("model");
            String color = rs.getString("color");
            long personId = rs.getLong("person_id");
            car = new Car(id, model, color, new Person(personId));
        }
        return car;
    }
}
