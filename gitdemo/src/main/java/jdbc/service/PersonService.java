package jdbc.service;

import jdbc.dao.CarDAO;
import jdbc.dao.PersonDAO;
import jdbc.model.Car;
import jdbc.model.Person;
import org.apache.log4j.Logger;

import java.sql.Statement;

public class PersonService {

    private static final Logger log = Logger.getLogger(PersonService.class);

    PersonDAO personDAO = new PersonDAO();
    CarDAO carDAO = new CarDAO();
    Statement statement;

    public Person findPersonById(long id, Statement statement) {
        return personDAO.findPersonById(id, statement);
    }

    /**
     * Метод позволяет добавить машину персоне
     * @return boolean, true если вставка в бд прошла успешна, иначе false
     * */
    public boolean buyCar(Person person, Car car, Statement statement) {
        log.info(String.format("Person: %s want to buy %d a car: %s", person.getName(), 1, car.getModel()));
        boolean personSuccess = personDAO.insertPerson(person, statement);
        boolean carSuccess = carDAO.insertCarForPerson(car, statement);

        Person personLast = personDAO.getLastInsertedPerson(statement);
        Car lastInsertedCar = carDAO.getLastInsertedCar(statement);
        personLast.setCar(lastInsertedCar);
        lastInsertedCar.setPerson(personLast);
        log.debug("Объекты без связей вставлены в таблицу");

        personDAO.updatePersonCarId(personLast.getId(), lastInsertedCar.getId(), statement);
        carDAO.updateCarPersonId(personLast.getId(), lastInsertedCar.getId(), statement);

        return personSuccess && carSuccess;
    }

    public void test(Person person) {
        person.setAge(46);
        // update bd
    }

    public void test() {
        Person person = personDAO.findPersonById(1L, statement);
        System.out.println("Person found " + person);
    }
}
