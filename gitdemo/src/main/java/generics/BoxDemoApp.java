package generics;

import java.util.Scanner;

public class BoxDemoApp {
    public static void main(String[] args) {
        SimpleBox intBox1 = new SimpleBox(20);
        SimpleBox intBox2 = new SimpleBox(30);
        // int long double ...   Integer Long Double ...
        if (intBox1.getObj() instanceof Integer
                && intBox2.getObj() instanceof Integer) {
            int sum = (int) intBox1.getObj() + (int) intBox2.getObj();
            System.out.println(sum);
        } else {
            System.out.println("Содержимое коробок различается");
        }

        intBox1.setObj("Java");

        System.out.println("End");


        String java = intBox1.showBoxItem("java");
        Object hello = intBox1.showBoxItemIncorrect("Hello");

    }
}
