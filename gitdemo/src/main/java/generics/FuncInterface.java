package generics;

@FunctionalInterface
public interface FuncInterface<T, V> {
    T accept(V obj2);
}
