package generics;

public class GenericDemoApp {
    public static void main(String[] args) {
        TestGeneric<String> testGeneric = new TestGeneric<>("Java");
        testGeneric.showType();
        System.out.println("testGeneric.getObj() = " + testGeneric.getObj());

        TestGeneric<Integer> testGeneric1 = new TestGeneric<>(140);
        testGeneric1.showType();
        System.out.println("testGeneric.getObj() = " + testGeneric1.getObj());

//        testGeneric.setObj(456);  Ошибка компиляции

    }
}
