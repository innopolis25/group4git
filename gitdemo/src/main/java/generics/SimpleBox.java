package generics;

public class SimpleBox {

    private Object obj;

    public SimpleBox(Object obj) {
        this.obj = obj;
    }


    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
        showBoxItem(obj);
        showBoxItemIncorrect(obj);
    }

    public <T> T showBoxItem(T object) {
        System.out.println("В коробке: " + object.toString());
        return object;
    }

    public Object showBoxItemIncorrect(Object o) {
        System.out.println("В коробке: " + o.toString());
        return o;
    }
}
