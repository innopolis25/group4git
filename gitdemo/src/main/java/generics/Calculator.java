package generics;

public class Calculator {
    public static int sumValues(int... values) {
        int result = 0;
        int i;
        for (i = 0; i < values.length; i++) {
            result = result + values[i];
        }
        return result;
    }


    public static int deductionValues(int... values) {
        int result = 0;
        int i;
        int max = Integer.MIN_VALUE;
        for (int value : values) {
            if (value > max) {
                max = value;
            }
        }
        for (i = 0; i < values.length; i++) {
            result = result + values[i];
        }
        return max + (max - result);
    }

    public static int multiplicationValues(int... values) {
        int result = 1;
        int i;
        for (i = 0; i < values.length; i++) {
            result = result * values[i];
        }
        return result;
    }

    public static String divisionValues(int... values) {
        int result = 0;
        String s = "Окончательный ответ деления: ";
        int i;
        int max = Integer.MIN_VALUE;
        for (int value : values) {
            if (value > max) {
                max = value;
            }
        }
        for (i = 0; i < values.length; i++) {
            result = result + values[i];
            if (max > values[i] && values[i] > 0) {
                max = max / values[i];
                System.out.println("Результат деления:" + max);
            }
        }
        return s + max;
    }


    public static int getFactorial(int f) {
        if (f < 0) {
            System.out.println("Пока");
            return -1;
        }
        int result = 1;
        for (int i = 1; i <= f; i++) {
            result = result * i;
        }
        return result;
    }

    public static int factorialValues (int[] values) {
        int result = 0;
        int i;
        for (i = 0; i < values.length; i++) {
            result += Calculator.getFactorial(values[i]);
            System.out.println("element " + values[i] + " factorial = " + result);
            result = 0;
        }
        return result;
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        int[] values = {1,2,3,4,5,6,7,8};
        factorialValues(values);
    }
}
