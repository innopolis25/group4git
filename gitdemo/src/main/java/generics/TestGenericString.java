package generics;

public class TestGenericString {

    private String obj;

    public TestGenericString(String obj) {
        this.obj = obj;
    }

    public String getObj() {
        return obj;
    }

    public void setObj(String obj) {
        this.obj = obj;
    }
}
