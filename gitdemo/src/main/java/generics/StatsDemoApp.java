package generics;

import oopAdvanced.Animal;
import oopAdvanced.Cat;

import java.util.function.Predicate;

public class StatsDemoApp {
    public static void main(String[] args) {
//        FuncInterface<Cat, String> lambda = s -> {
//            return new Cat(s);
//        };
//        Predicate

        Stats<Integer> intStats = new Stats<>(1,2,3,4);
        Stats<Number> doubleStats = new Stats<>(2.0,3.0);
        System.out.println(intStats.sum());
        System.out.println(doubleStats.sum());

        System.out.println(intStats.sumAll(doubleStats));

        Stats stats = new Stats();

    }
}
