package generics;

public class TwoGenApp {
    public static void main(String[] args) {
        TwoGen<Integer, String> twoGenObj = new TwoGen<>(123, "Java");
        twoGenObj.showType();

        int intValue = twoGenObj.getObj1();
        String stringValue = twoGenObj.getObj2();
        System.out.println(intValue);
        System.out.println(stringValue);
    }
}
