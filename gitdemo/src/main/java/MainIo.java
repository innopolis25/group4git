import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainIo {
    public static void main(String[] args) throws IOException {
//        Path path1 = Paths.get("text.txt");
//        System.out.println(path1.toString());
//        Path directory = Files.createDirectory(Paths.get("123"));
//        System.out.println(directory);
        File file = new File("text.txt");
        BufferedReader bf = new BufferedReader(new FileReader(file));
        String s = bf.readLine();
        System.out.println(s);
    }
}
