package oopAdvanced;

public class HomeCat extends Cat {
    // Animal -> Cat -> HomeCat

    String address;

    public HomeCat(String name, String color) {
        super(name, color);
    }

    public HomeCat(String name, String color, String address) {
        super(name, color);
        this.address = address;
    }

    public HomeCat(String name, String color, int age) {
        super(name, color, age);
    }

    public void printSmt() {
        super.getAgeInt();
        System.out.println("Printed");
    }

    public void printSmt(int value) {
        System.out.println("Another " + value);
    }

    public void printSmt(Animal a) {}

    public void printSmt(Cat c) {}

    @Override
    public void eat() {
        System.out.println("HomeCat");
    }
}
