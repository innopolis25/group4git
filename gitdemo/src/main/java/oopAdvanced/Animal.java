package oopAdvanced;

import java.lang.*;

public abstract class Animal implements Moveable {

    String name;
    private int num;
    static int count;

    static final int CONST = 10;

    {

    }

    static {

    }

    public Animal(String name) {
        this.name = name;
        count++;
    }

    public void animalInfo() {
        System.out.println("Animal : " + name);
    }

    protected void eat() {
        System.out.println("I'm eating. My name is " + name);
    }

    public final void foo() {}

    abstract void jump();
}
