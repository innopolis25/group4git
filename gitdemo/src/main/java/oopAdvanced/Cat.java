package oopAdvanced;

public class Cat extends Animal {

    // public -> default -> protected -> private

    String color;
    int age;
    final int[] array = new int[10];
//    final Animal animal = new HomeCat("f","s");

    // Heap, Stack, Garbage Collector


    public Cat(String name, String color) {
        super(name);
        this.color = color;
        array[0] = 10;
        array[0] = 17;
        array[4] = 10;
//        animal.animalInfo();
    }

    public Cat(String name, String color, int age) {
        super(name);
        this.color = color;
        this.age = age;
    }

    public void catInfo() {
        System.out.println("Cat: " + name + "color: " + color);
    }

    int getAgeInt() {
        return age;
    }

    @Override
    public void eat() {
        System.out.println("asdasd");
    }

    @Override
    void jump() {

    }

    @Override
    public void move() {
        int x = 10;
        Car car = new Car();
        System.out.println("Cat moving");
    }

    public static void newMethod(final int x) {

        System.out.println("static method");
    }
}
