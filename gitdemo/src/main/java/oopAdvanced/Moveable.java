package oopAdvanced;

public interface Moveable {

    public static final String NAME = "name";

    void move();
    // stream API // Collection // lambda

    default void foo() {
        System.out.println("Default method");
        printF();
    }

    private void printF() {
        System.out.println("Private method");
    }

}
