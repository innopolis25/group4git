package lesson3;

import java.util.Arrays;

public class Main {

    static int b;

    private boolean condition3;

    public static void main(String[] args) {
        int[] array = new int[10];
        Arrays.fill(array, 5);

        int count = 20;
        int j = 0;
        for (int i = 0; i < count; i++) {

            if (i % array.length == 0) {
                j = 0;
            }

            array[j] += 2;
            j++;

        }

        // {1,2,3,4,5,6,7,8,9} 6

        System.out.println(Arrays.toString(array));
        System.out.println(array);
    }



    public static void foo2(int val) {
        System.out.println("hello");
        val = 10 + 5;
    }

    public static int foo3(int val) {
        return val + 10;
    }


}
