package streamApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MainStreamApi {

    static class Person {

        enum Position {
            ENGINEER, DIRECTOR, MANAGER;
        }

        private String name;
        private int age;
        private Position position;

        public Person(String name, int age, Position position) {
            this.name = name;
            this.age = age;
            this.position = position;
        }

        public Person(String name) {
            this.name = name;
        }
    }
    public static void main(String[] args) {
//        List<Person> persons = new ArrayList<>(Arrays.asList(
//                new Person("Bob1", 13, Person.Position.MANAGER),
//                new Person("Bob2", 45, Person.Position.DIRECTOR),
//                new Person("Bob3", 15, Person.Position.ENGINEER),
//                new Person("Bob4", 18, Person.Position.ENGINEER),
//                new Person("Bob5", 73, Person.Position.MANAGER),
//                new Person("Bob6", 59, Person.Position.DIRECTOR)
//        ));
//
//        List<String> collect = persons.stream()
//                .filter(p -> p.position == Person.Position.ENGINEER)
//                .sorted((o1, o2) -> o1.age - o2.age)
//                .map((Function<Person, String>) p -> p.name)
//                .collect(Collectors.toList());
//        System.out.println(collect);
//
//        int[] array = {1,2,3,4,5};
//
//        Arrays.stream(array).forEach(System.out::println);
//
//        IntStream streamInt = Stream.of(1,2,3,4).mapToInt(n -> n);
//
//        String[] stringArray = {"Aaa", "Bbbbbb", "Cc", "Aa"};
//
//        String a = Arrays.stream(stringArray)
//                .filter(s -> s.startsWith("A"))
//                .collect(Collectors.joining(" | ", "Перечисленные слова [", "" +
//                        "] начинаются на А"));
//        System.out.println(a);
//
//        Arrays.stream(stringArray).forEach(s -> {
//            System.out.println(s.length());
//        });
//
//        for (String s : stringArray) {
//            System.out.println(s.length());
//        }
//
//        IntStream streamInteger = IntStream.of(1,2,3,4,5,5,6,6,3,3,2,1,7);
//        streamInteger
//                .distinct()
//                .forEach(System.out::print);
//
//        List<Integer> intsList = new ArrayList<>();
//        intsList.add(1);
//        intsList.stream().collect(Collectors.toSet());

//        Stream<String> strStream = Stream.of("Java", "Core","Hello World");
//        strStream.map(Person::new).forEach(System.out::println); // Map<Key, Value>

//        Stream.of("dd2", "aa2", "bb1", "bb3", "cc4")
//                .filter(s -> {
//                    System.out.println("Фильтр: " + s);
//                    return s.startsWith("a");
//                })
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("Результат: " + s));
//
        Arrays.asList("a1", "a2", "b1", "b2", "c2", "c1")
                .parallelStream()
                .filter(s -> {
                    System.out.println("filter: " + Thread.currentThread().getName());
                    return true;
                })
                .forEach(s -> {
                    System.out.println("ForEach: " + Thread.currentThread().getName());
                });



    }
}
