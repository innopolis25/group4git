create table if not exists person
(
    id   serial primary key, -- sequence
    name varchar(255),
    age  integer
);

create table if not exists car
(
    id serial primary key,
    model varchar(255),
    color varchar(255)
);

alter table person add column car_id bigint;

alter table car add column person_id bigint;

alter table person add constraint car_id
    foreign key (car_id) references car(id);

alter table car add constraint person_id
    foreign key (person_id) references person(id);

-- ctrl + enter

drop table person;

truncate table person;