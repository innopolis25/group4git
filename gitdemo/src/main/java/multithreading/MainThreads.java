package multithreading;

import java.util.concurrent.TimeUnit;

public class MainThreads {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            Thread.yield();
            for (int i = 0; i <= 500; i++) {
//                try {
//                    TimeUnit.MILLISECONDS.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                if (i == 500) {
                    System.out.println("Thread1 " + i);
                }
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i <= 500; i++) {
//                try {
//                    TimeUnit.MILLISECONDS.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                if (i == 500) {
                    System.out.println("Thread2 " + i);
                }
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();

        Thread th2 = new Thread(new Thread(t2));
        th2.start();
        System.out.println("END");

    }
}
