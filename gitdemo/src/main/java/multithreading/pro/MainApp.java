package multithreading.pro;

public class MainApp {

    public static class Counter {

        private int first;
        private int second;

        private final Object monFirst = new Object();
        private final Object monSecond = new Object();

        public int getFirst() {
            return first;
        }

        public int getSecond() {
            return second;
        }

        public void incrementfirst() {
            synchronized (monFirst) {
                first++;
            }
        }

        public void decrementfirst() {
            synchronized (monFirst) {
                first--;
            }
        }

        public void incrementsecond() {
            synchronized (monSecond) {
                second++;
            }
        }

        public void decrementsecond() {
            synchronized (monSecond) {
                second--;
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                counter.incrementfirst();
                counter.incrementsecond();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                counter.decrementfirst();
                counter.decrementsecond();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();


        System.out.println(counter.getFirst() + ":" + counter.getSecond());
    }
}
