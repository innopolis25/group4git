package multithreading.pro;

import java.util.concurrent.TimeUnit;

// ABABABABAB
public class PrintABApp {
    private final Object mon = new Object();
    private volatile char currentLetter = 'A';

    public static void main(String[] args) {
        PrintABApp waitNotifyDemo = new PrintABApp();
        new Thread(() -> {
            waitNotifyDemo.printA();
        }).start();
        Thread thread = new Thread(() -> {
            waitNotifyDemo.printB();
        });
        thread.start();
        thread.interrupt();
    }

    public synchronized void printA() {
            try {
                for (int i = 0; i < 10; i++) {
                    while (currentLetter != 'A') {
                        wait();
                    }
                    TimeUnit.MILLISECONDS.sleep(500);
                    System.out.print("A");
                    currentLetter = 'B';
                    notify();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public synchronized void printB() {
            try {
                for (int i = 0; i < 10; i++) {
                    while(currentLetter != 'B') {
                        wait();
                    }
                    TimeUnit.MILLISECONDS.sleep(500);
                    System.out.print("B");
                    currentLetter = 'A';
                    notify();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
