package multithreading.pro;

import java.util.concurrent.*;

public class ExecutorServices {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService service2 = Executors.newFixedThreadPool(4);
        Future<String> future = service2.submit(() -> {
                System.out.println("Submit");
                int x = 10 / 5;
                return "Поток завершен";
            });

        try {
            System.out.println(future.get());
        } catch (ExecutionException e) {
            System.out.println("Error: " + e.getMessage());
        }
        System.out.println(future.get());

        service2.shutdown();

    }

}
