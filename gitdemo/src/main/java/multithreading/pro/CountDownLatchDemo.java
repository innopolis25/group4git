package multithreading.pro;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException, IOException {
        final int THREADS_COUNT = 6;
        final CountDownLatch cdl = new CountDownLatch(THREADS_COUNT);
        Path path = Paths.get("gitdemo/text.txt");

        System.out.println("Start");
        for (int i = 0; i < THREADS_COUNT; i++) {
            final int w = i;

            new Thread(() -> {
                try {
                    TimeUnit.SECONDS.sleep(1);
                    cdl.countDown();
                    Files.writeString(path, "Поток " + w + " готов\n", StandardOpenOption.APPEND);
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }

        cdl.await();
        Files.writeString(path, "End of main\n", StandardOpenOption.APPEND);

        // Maven Gradle

    }
}
