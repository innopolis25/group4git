package multithreading;

import java.util.concurrent.TimeUnit;

public class MyRunnable implements Runnable{

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("hello");
    }
}
