package multithreading;

public class MyThread extends Thread{


    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            if (i == 8) {
                throw new IllegalThreadStateException();
            }
            System.out.println("Count: " + i + ", " + Thread.currentThread().getName());
        }
    }


}
