package hwAp;

public abstract class Animals {
    String name;
    int run, swim;
    static int count;

    public Animals(String name, int run, int swim) {
        this.name = name;
        this.run = run;
        this.swim = swim;
    }

    public void run(int distance) {
        if (run < distance) {
            System.out.println("Кот " + this.name + " из " + this.run + " метров " + "пробежал только " + distance + " метров.");
        } else {
            System.out.println("Кот " + this.name + " пробежал " + distance + " метров.");
            run-=distance;
        }
    }

    public void swim() {
        if (swim < 10) {
            System.out.println("Собака " + this.name + " проплыла " + swim + " метров.");
        } else {
            System.out.println("Собака " + this.name + " смогла проплыть только 10 метров, а дальше спасатели МЧС вернули собаку на сушу.");
        }
    }
}
