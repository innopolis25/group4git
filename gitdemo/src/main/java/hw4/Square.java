package hw4;

public class Square extends Rectangle implements Moveable{
    @Override
    int getPerimeter() {
        return (x + y) * 2;
    }

    @Override
    public void move(int newX, int newY) {
        x = newX;
        y = newY;
    }
}
