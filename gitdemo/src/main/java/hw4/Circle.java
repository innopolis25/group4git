package hw4;

public class Circle extends Ellipse implements Moveable{

    @Override
    int getPerimeter() {
        return (int) (2 * 3.14 * x);
    }

    @Override
    public void move(int newX, int newY) {
        x = newX;
        y = newY;
    }

}
