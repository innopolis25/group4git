package hw4;

public class Main {
    public static void main(String[] args) {

        Figure[] figures = new Figure[3];
        figures[0] = new Square();
        figures[1] = new Circle();
        figures[2] = new Ellipse();
        for (Figure figure : figures) {
            if (figure instanceof Moveable){
                ((Moveable) figure).move(1,1);
            }
            System.out.println(figure.getPerimeter());
        }
    }
}
