package hw4;

public interface Moveable {
    void move(int x, int y);
}
