package homework7.dao;

import homework7.model.User;

public interface UsersRepositoryFile {
    User findById(int id);
    void create(User user);
    void update(User user);
    void delete(int id);
}
