package homework7.dao;

import homework7.model.User;
import homework7.service.FileService;
import homework7.service.UserService;

public class UsersRepositoryFileImpl implements UsersRepositoryFile{

    FileService fileService = new FileService();
    UserService userService = new UserService();


    @Override
    public User findById(int id) {
        String s = fileService.readDataFromFile(id);
        User user = userService.convertStringToUser(s);
        return user;
    }

    @Override
    public void create(User user) {

    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(int id) {

    }

}
