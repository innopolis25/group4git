package homework7.model;

public class User {

    private int id;

    private String name;

    private String lastName;

    private int age;

    private boolean isWorking;

    public User(String name, String lastName, int age, boolean isWorking) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.isWorking = isWorking;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorking() {
        return isWorking;
    }

    public void setWorking(boolean working) {
        isWorking = working;
    }
}
