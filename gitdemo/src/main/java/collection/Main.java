package collection;

import hw4.Circle;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        map.put("four", 4);

        Map<Human, String> humans = new HashMap<>();
        Human human1 = new Human(45, "Nick");
        Human human2 = new Human(48, "Nick");

        humans.put(human1, "1234");
        humans.put(human2, "5678");

        Human human3 = new Human(45, "Nick");
        String nick = humans.get(human3);
        System.out.println("asdasdasd = " + nick);
        System.out.println(humans);
    }

    public void foo(List<Integer> ints) {

    }
}
