package collection;

import java.util.Arrays;

public class MyArrayList<T> {

    private Object[] array;
    private int capacity;
    private int size;

    public MyArrayList() {
        capacity = 10;
        size = 0;
        array = new Object[capacity];
    }

    public void add(T elem) {
        if ((double) (size / capacity) > 0.75) {
            ensureCapacity();
        }
        array[size] = elem;
        size++;
    }

    private void ensureCapacity() {
        capacity *= 1.5;
        Object[] newArr = new Object[capacity];
        System.arraycopy(array, 0, newArr, 0, array.length);
        array = newArr;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        Object[] newArray = new Object[size];
        System.arraycopy(array, 0, newArray, 0, size);
        return Arrays.toString(newArray);
    }
}
