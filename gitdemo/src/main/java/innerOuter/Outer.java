package innerOuter;

import hw4.Moveable;

public class Outer {
    private int y;

    public static void main(String[] args) {
        InnerStatic.staticMethod();
    }



    private static class InnerStatic implements Moveable {
        private int x;

        private static void staticMethod() {
            System.out.println("in static");
        }

        void foo() {
            Outer outer = new Outer();
            int y = outer.y;

        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        @Override
        public void move(int x, int y) {

        }

    }

    public int getY() {
        InnerStatic innerStatic = new InnerStatic();
        innerStatic.getX();
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
