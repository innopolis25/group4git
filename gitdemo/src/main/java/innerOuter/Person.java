package innerOuter;

public class Person {
    int age;
    String name;
    Child2 child2 = new Child2();

    public static class Parent1{
        String name;
        int age;
    }

    public static class Parent2{
        String name;
        int age;
    }

    public static class Child1{
        String name;
        int age;
    }
}
