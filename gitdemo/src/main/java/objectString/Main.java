package objectString;

import lambda.LapTop;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String str = "string"; // изменяемые/неизменяемые
        char[] chars = str.toCharArray();
        chars[2] = 0;
        String strChanged = new String(chars);
        System.out.println(Arrays.toString(chars));
        System.out.println(strChanged);
        String changed = str + "asd";

        String s1 = new String("asdasd");
        String s3 = changed;
        int x = 10;
        String s4 = String.valueOf(x);
        byte[] byteArray = {0,65,3,4};
        String s5 = new String(byteArray, StandardCharsets.UTF_16);
        System.out.println(s5);

        String a1 = "hello";
        String a2 = "world";
        String a3 = a1 + a2;

        String a4 = "hello" + (5 + 5);
        // String.valueOf(5)
        System.out.println(a4);
        // String-pool

        LapTop lapTop = new LapTop(); // куча(heap) - стековая память(stack)
        lapTop = new LapTop();
        String newStr = "hello"; // newStr = a1; новая строка не создается
        String newStrNew = new String(a1);

        String lapTopStr = lapTop.toString();
        System.out.println(lapTopStr);

        StringBuilder correctAppend = new StringBuilder("string");
        for (int i = 0; i < 1000; i++) {
            correctAppend.append(i);
        }

        // + garbage collector
    }
}
