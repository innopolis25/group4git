package oop;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Bob",24,"M"); // person = address -> object
        changeAge(person);
        System.out.println(person.getAge());
        person.setAge(45);

    }

    public static void changeAge(Person copyPerson) {
        // Person copyPerson = person;
        copyPerson.setAge(16);
    }

    public static int changeValue(int xyz) {
        xyz = xyz + 10;
        System.out.println("changeValue: " + xyz);
        return xyz;
    }

}
