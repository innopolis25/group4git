package staticc;

public class Main {
    static int x;
    int y;

    public static void main(String[] args) {
        foo2();

        Main mainn = new Main();
        mainn.foo1();

        StaticTest test = new StaticTest();
        test.setNonStatic("asdf");
    }

    void foo1(){
        x++;
        y++;
    }

    static void foo2() {
        x++;
    }

}
