package staticc;

public class Person {

    private String name;

    private int age;

    static String countOfNose;

    static {
        countOfNose = "one";
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountOfNose() {
        return countOfNose;
    }

    public static void main(String[] args) {
        Person nick = new Person("Nick", 25);
        System.out.println(nick);
    }

}
