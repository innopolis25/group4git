package staticc;

public class Initialization {

    private int x;
    private static int y;
    private static String s;
    private static Person person;

    // 1
    {
        System.out.println("Init block x=" + x + ", y=" + y);
        x = 10;
        y = 20;
        s = "init block";
    }

    // 2
    static {
        System.out.println("Static init block y=" + y + ", s=" + s);
//        s.toUpperCase(); // null.toUpperCase();
        y = 25;
        s = "after Static";
        person = new Person("Nick", 170);
    }

    // 3
    public Initialization() {
        System.out.println("In Constructor" + x + ", y=" + y);
        x = 30;
        y = 50;
    }

    public static void main(String[] args) {
        Initialization initialization = new Initialization();
        System.out.println(initialization);
        Person person = initialization.getPerson();
        System.out.println(person.getCountOfNose());
    }

    public Person getPerson() {
        return person;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
