package staticc;

public class StaticTest {

    private static String field;

    private String nonStatic;

    private static int count;

    public StaticTest() {
        count++;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        StaticTest.count = count;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getNonStatic() {
        return nonStatic;
    }

    public void setNonStatic(String nonStatic) {
        this.nonStatic = nonStatic;
    }
}
